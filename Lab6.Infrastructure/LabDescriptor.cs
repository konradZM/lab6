﻿using Lab6.ControlPanel.Contract;
using PK.Container;
using System;
using System.Reflection;
using Lab.NetContainer;

namespace Lab6.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1

        public static Func<IContainer> ContainerFactory = () => new Container();

        public static Assembly ControlPanelSpec = Assembly.GetAssembly(typeof(IControlPanel));
        public static Assembly ControlPanelImpl = Assembly.GetAssembly(typeof(Lab6.ControlPanel.Implementation.Panel));

        public static Assembly MainComponentSpec = Assembly.GetAssembly(typeof(Lab6.MainComponent.Contract.IGlownyKomp));
        public static Assembly MainComponentImpl = Assembly.GetAssembly(typeof(Lab6.MainComponent.Implementation.GlownyKomp));

        #endregion
    }
}
