﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

using Lab6.MainComponent.Contract;

namespace Lab6.ControlPanel.Implementation
{
    public class Panel : Lab6.ControlPanel.Contract.IControlPanel
    {
        private Window okno;

        public Panel(IGlownyKomp glownykomp)
        {
            this.okno = new Window1();
        }

        public System.Windows.Window Window
        {
            get { return this.okno; }
        }
    }
}
